package com.isemenov.snippets.dao;

import javax.persistence.EntityManager;

public class EmptyDAO {

    private EntityManager manager;

    public EmptyDAO(EntityManager manager) {
        this.manager = manager;
    }

    protected EntityManager getManager() {
        return manager;
    }
}
