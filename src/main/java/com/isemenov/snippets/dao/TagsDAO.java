package com.isemenov.snippets.dao;

import com.isemenov.snippets.domain.Tag;

import javax.persistence.EntityManager;
import java.util.List;

public class TagsDAO extends EmptyDAO {

    public TagsDAO(EntityManager em) {
        super(em);
    }

    public Tag createTag(String name){
        Tag tag = new Tag(name);
        getManager().getTransaction().begin();
        getManager().persist(tag);
        getManager().getTransaction().commit();
        return tag;
    }

    public void deleteTag(Tag tag){
        getManager().getTransaction().begin();
        getManager().remove(tag);
        getManager().getTransaction().commit();
    }

    public Tag updateTag(Tag tag){
        getManager().getTransaction().begin();
        Tag merged = getManager().merge(tag);
        getManager().getTransaction().commit();
        return merged;
    }

    public Tag findById(long id){
        return getManager().createNamedQuery("findTagById", Tag.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    public Tag findByName(String name){
        return getManager().createNamedQuery("findTagByName", Tag.class)
                .setParameter("name", name)
                .getSingleResult();
    }

    public List<Tag> findAll(){
        return getManager().createNamedQuery("findAllTags", Tag.class)
                .getResultList();
    }
}
