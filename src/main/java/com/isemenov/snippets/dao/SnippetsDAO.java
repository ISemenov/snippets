package com.isemenov.snippets.dao;

import com.isemenov.snippets.domain.Snippet;
import com.isemenov.snippets.domain.User;
import com.isemenov.snippets.domain.referenceBooks.AccessLevel;

import javax.persistence.EntityManager;
import java.util.List;

public class SnippetsDAO extends EmptyDAO {

    public SnippetsDAO(EntityManager em) {
        super(em);
    }

    public Snippet createSnippet(String description, String fileLink, AccessLevel accessLevel, User user){
        Snippet snippet = new Snippet(description, fileLink, accessLevel, user);
        getManager().getTransaction().begin();
        getManager().persist(snippet);
        getManager().getTransaction().commit();
        return snippet;
    }

    public void deleteSnippet(Snippet snippet){
        getManager().getTransaction().begin();
        getManager().remove(snippet);
        getManager().getTransaction().commit();
    }

    public Snippet updateSnippet(Snippet snippet){
        getManager().getTransaction().begin();
        Snippet merged = getManager().merge(snippet);
        getManager().getTransaction().commit();
        return merged;
    }

    public Snippet findById(long id){
        return getManager().createNamedQuery("findSnippetById", Snippet.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    public List<Snippet> findByUser(User user){
        return getManager().createNamedQuery("findAllSnippetsByUser", Snippet.class)
                .setParameter("user", user)
                .getResultList();
    }
}
