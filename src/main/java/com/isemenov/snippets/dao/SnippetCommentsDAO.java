package com.isemenov.snippets.dao;

import com.isemenov.snippets.domain.Snippet;
import com.isemenov.snippets.domain.SnippetComment;
import com.isemenov.snippets.domain.User;

import javax.persistence.EntityManager;
import java.util.List;

public class SnippetCommentsDAO extends EmptyDAO {

    public SnippetCommentsDAO(EntityManager em) {
        super(em);
    }

    public SnippetComment createSnippetComment(String message, User user, Snippet snippet){
        getManager().getTransaction().begin();
        SnippetComment snippetComment = new SnippetComment(message, user, snippet);
        getManager().persist(snippetComment);
        getManager().getTransaction().commit();
        return snippetComment;
    }

    public void deleteSnippetComment(SnippetComment snippetComment){
        getManager().getTransaction().begin();
        getManager().remove(snippetComment);
        getManager().getTransaction().commit();
    }

    public SnippetComment updateSnippetComment(SnippetComment snippetComment){
        getManager().getTransaction().begin();
        SnippetComment merged = getManager().merge(snippetComment);
        getManager().getTransaction().commit();
        return merged;
    }

    public SnippetComment findById(long id){
        return getManager().createNamedQuery("findSnippetCommentById", SnippetComment.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    public List<SnippetComment> findByUser(User user){
        return getManager().createNamedQuery("findAllSnippetCommentsByUser", SnippetComment.class)
                .setParameter("user", user)
                .getResultList();
    }

    public List<SnippetComment> findBySnippet(Snippet snippet){
        return getManager().createNamedQuery("findAllSnippetCommentsBySnippet", SnippetComment.class)
                .setParameter("snippet", snippet)
                .getResultList();
    }
}
