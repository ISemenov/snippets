package com.isemenov.snippets.dao.referenceBooks;

import com.isemenov.snippets.dao.EmptyDAO;
import com.isemenov.snippets.domain.referenceBooks.Language;

import javax.persistence.EntityManager;
import java.util.List;

public class LanguagesDAO extends EmptyDAO {

    public LanguagesDAO(EntityManager em) {
        super(em);
    }

    public Language createLanguage(String name, String code){
        Language language = new Language(name, code);
        getManager().getTransaction().begin();
        getManager().persist(language);
        getManager().getTransaction().commit();
        return language;
    }

    public void deleteLanguage(Language language){
        getManager().getTransaction().begin();
        getManager().remove(language);
        getManager().getTransaction().commit();
    }

    public Language updateLanguage(Language language){
        getManager().getTransaction().begin();
        Language merged = getManager().merge(language);
        getManager().getTransaction().commit();
        return merged;
    }

    public Language findById(long id){
        return getManager().createNamedQuery("findLanguageById", Language.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    public Language findByName(String name){
        return getManager().createNamedQuery("findLanguageByName", Language.class)
                .setParameter("name", name)
                .getSingleResult();
    }

    public Language findByCode(String code){
        return getManager().createNamedQuery("findLanguageByCode", Language.class)
                .setParameter("code", code)
                .getSingleResult();
    }

    public List<Language> findAll(){
        return getManager().createNamedQuery("findAllLanguages", Language.class)
                .getResultList();
    }
}
