package com.isemenov.snippets.dao.referenceBooks;

import com.isemenov.snippets.dao.EmptyDAO;
import com.isemenov.snippets.domain.referenceBooks.AccessLevel;

import javax.persistence.EntityManager;
import java.util.List;

public class AccessLevelsDAO extends EmptyDAO {

    public AccessLevelsDAO(EntityManager em) {
        super(em);
    }

    public AccessLevel createAccessLevel(String name, String code){
        AccessLevel accessLevel = new AccessLevel(name, code);
        getManager().getTransaction().begin();
        getManager().persist(accessLevel);
        getManager().getTransaction().commit();
        return accessLevel;
    }

    public void deleteAccessLevel(AccessLevel accessLevel){
        getManager().getTransaction().begin();
        getManager().remove(accessLevel);
        getManager().getTransaction().commit();
    }

    public AccessLevel updateAccessLevel(AccessLevel accessLevel){
        getManager().getTransaction().begin();
        AccessLevel merged = getManager().merge(accessLevel);
        getManager().getTransaction().commit();
        return merged;
    }

    public AccessLevel findById(long id){
        return getManager().createNamedQuery("findAccessLevelById", AccessLevel.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    public AccessLevel findByName(String name){
        return getManager().createNamedQuery("findAccessLevelByName", AccessLevel.class)
                .setParameter("name", name)
                .getSingleResult();
    }

    public AccessLevel findByCode(String code){
        return getManager().createNamedQuery("findAccessLevelByCode", AccessLevel.class)
                .setParameter("code", code)
                .getSingleResult();
    }

    public List<AccessLevel> findAll(){
        return getManager().createNamedQuery("findAllAccessLevels", AccessLevel.class)
                .getResultList();
    }
}
