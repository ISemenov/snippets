package com.isemenov.snippets.dao.referenceBooks;

import com.isemenov.snippets.dao.EmptyDAO;
import com.isemenov.snippets.domain.referenceBooks.Role;

import javax.persistence.EntityManager;
import java.util.List;

public class RolesDAO extends EmptyDAO {

    public RolesDAO(EntityManager em) {
        super(em);
    }

    public Role createRole(String name, String code){
        Role role = new Role(name, code);
        getManager().getTransaction().begin();
        getManager().persist(role);
        getManager().getTransaction().commit();
        return role;
    }

    public void deleteRole(Role role){
        getManager().getTransaction().begin();
        getManager().remove(role);
        getManager().getTransaction().commit();
    }

    public Role updateRole(Role role){
        getManager().getTransaction().begin();
        Role merged = getManager().merge(role);
        getManager().getTransaction().commit();
        return merged;
    }

    public Role findById(long id){
        return getManager().createNamedQuery("findRoleById", Role.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    public Role findByName(String name){
        return getManager().createNamedQuery("findRoleByName", Role.class)
                .setParameter("name", name)
                .getSingleResult();
    }

    public Role findByCode(String code){
        return getManager().createNamedQuery("findRoleByCode", Role.class)
                .setParameter("code", code)
                .getSingleResult();
    }

    public List<Role> findAll(){
        return getManager().createNamedQuery("findAllRoles", Role.class)
                .getResultList();
    }
}
