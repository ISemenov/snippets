package com.isemenov.snippets;

import com.isemenov.snippets.dao.UsersDAO;
import com.isemenov.snippets.domain.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class App {

    public static void main(String[] args) {

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("TestPersistenceUnit");
        EntityManager em = factory.createEntityManager();
        UsersDAO usersDAO = new UsersDAO(em);
//        EntityTransaction transaction = em.getTransaction();
//        transaction.begin();
//
//        User user = new User("test", "test", "test");
//        em.persist(user);
//
//        em.getTransaction().commit();

    }
}
