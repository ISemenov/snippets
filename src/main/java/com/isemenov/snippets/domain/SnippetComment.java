package com.isemenov.snippets.domain;

import com.isemenov.snippets.domain.base.BaseComment;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

import static com.isemenov.snippets.domain.SnippetComment.FIND_SNIPPET_COMMENT_BY_ID_QUERY;
import static com.isemenov.snippets.domain.SnippetComment.FIND_SNIPPET_COMMENTS_BY_USER_QUERY;
import static com.isemenov.snippets.domain.SnippetComment.FIND_SNIPPET_COMMENTS_BY_SNIPPET_QUERY;

@NamedQueries({
        @NamedQuery(name = FIND_SNIPPET_COMMENT_BY_ID_QUERY, query = "from SnippetComment where id = :id"),
        @NamedQuery(name = FIND_SNIPPET_COMMENTS_BY_USER_QUERY, query = "from SnippetComment where user = :user"),
        @NamedQuery(name = FIND_SNIPPET_COMMENTS_BY_SNIPPET_QUERY, query = "from SnippetComment where snippet = :snippet")
    }
)
@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "comments")
public class SnippetComment extends BaseComment {

    public static final String FIND_SNIPPET_COMMENT_BY_ID_QUERY = "findSnippetCommentById";
    public static final String FIND_SNIPPET_COMMENTS_BY_USER_QUERY = "findAllSnippetCommentsByUser";
    public static final String FIND_SNIPPET_COMMENTS_BY_SNIPPET_QUERY = "findAllSnippetCommentsBySnippet";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "snippet_comment_id_generator")
    @SequenceGenerator(name = "snippet_comment_id_generator", sequenceName = "snippet_comment_id_seq")
    @Column(name = "snippet_comment_id")
    private long snippetCommentId;

    @ManyToOne
    @JoinColumn(name = "snippet_id")
    private Snippet snippet;

    public SnippetComment(String message, User user, Snippet snippet) {
        super(message, user);
        this.snippet = snippet;
    }
}
