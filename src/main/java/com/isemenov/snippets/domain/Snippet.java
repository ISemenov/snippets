package com.isemenov.snippets.domain;

import com.isemenov.snippets.domain.referenceBooks.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.isemenov.snippets.domain.Snippet.FIND_SNIPPET_BY_ID_QUERY;
import static com.isemenov.snippets.domain.Snippet.FIND_SNIPPETS_BY_USER_QUERY;

@NamedQueries({
        @NamedQuery(name = FIND_SNIPPET_BY_ID_QUERY, query = "from Snippet where id = :id"),
        @NamedQuery(name = FIND_SNIPPETS_BY_USER_QUERY, query = "from Snippet where user = :user")
    }
)
@Getter
@Setter
@Entity
@Table(name = "snippets")
public class Snippet {

    public static final String FIND_SNIPPET_BY_ID_QUERY = "findSnippetById";
    public static final String FIND_SNIPPETS_BY_USER_QUERY = "findAllSnippetsByUser";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "snippet_id_generator")
    @SequenceGenerator(name = "snippet_id_generator", sequenceName = "snippet_id_seq")
    @Column(name = "snippet_id")
    private long snippetId;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "file_link", nullable = false, unique = true)
    private String fileLink;

    @Column(name = "likes_rating", nullable = false)
    private double likesRating;

    @Column(name = "usage_rating", nullable = false)
    private double usageRating;

    @OneToMany(mappedBy = "snippet", cascade = CascadeType.ALL)
    private List<SnippetComment> snippetComments;

    @ManyToOne
    @JoinColumn(name = "access_level_id")
    private AccessLevel accessLevel;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "snippets_tags",
            joinColumns = @JoinColumn(name = "snippet_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    private Collection<Tag> tags;

    public Snippet() {
        this.likesRating = 0D;
        this.usageRating = 0D;
        this.tags = new ArrayList<>();
        this.snippetComments = new ArrayList<>();
    }

    public Snippet(String description, String fileLink, AccessLevel accessLevel, User user) {
        this();
        this.description = description;
        this.fileLink = fileLink;
        this.user = user;
        this.accessLevel = accessLevel;
    }

    public Snippet(String description, String fileLink, AccessLevel accessLevel, User user, Collection<Tag> tags) {
        this();
        this.description = description;
        this.fileLink = fileLink;
        this.accessLevel = accessLevel;
        this.user = user;
        this.tags = tags;
    }
}
