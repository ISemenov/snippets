package com.isemenov.snippets.domain.referenceBooks;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

import static com.isemenov.snippets.domain.referenceBooks.Language.FIND_LANGUAGE_BY_ID_QUERY;
import static com.isemenov.snippets.domain.referenceBooks.Language.FIND_LANGUAGE_BY_NAME_QUERY;
import static com.isemenov.snippets.domain.referenceBooks.Language.FIND_LANGUAGE_BY_CODE_QUERY;
import static com.isemenov.snippets.domain.referenceBooks.Language.FIND_LANGUAGES_QUERY;

@NamedQueries({
        @NamedQuery(name = FIND_LANGUAGE_BY_ID_QUERY, query = "from Language where id = :id"),
        @NamedQuery(name = FIND_LANGUAGE_BY_NAME_QUERY, query = "from Language where name = :name"),
        @NamedQuery(name = FIND_LANGUAGE_BY_CODE_QUERY, query = "from Language where code = :code"),
        @NamedQuery(name = FIND_LANGUAGES_QUERY, query = "from Language")
    }
)
@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "languages")
public class Language extends ReferenceBook{

    public static final String FIND_LANGUAGE_BY_ID_QUERY = "findLanguageById";
    public static final String FIND_LANGUAGE_BY_NAME_QUERY = "findLanguageByName";
    public static final String FIND_LANGUAGE_BY_CODE_QUERY = "findLanguageByCode";
    public static final String FIND_LANGUAGES_QUERY = "findAllLanguages";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "language_id_generator")
    @SequenceGenerator(name = "language_id_generator", sequenceName = "language_id_seq")
    @Column(name = "language_id")
    private long languageId;

    public Language(String name, String code) {
        super(name, code);
    }
}
