package com.isemenov.snippets.domain.referenceBooks;

import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@NoArgsConstructor
@MappedSuperclass
class ReferenceBook {

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Column(name = "code", nullable = false, unique = true)
    private String code;

    ReferenceBook(String name, String code) {
        this.name = name;
        this.code = code;
    }
}
