package com.isemenov.snippets.domain.referenceBooks;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

import static com.isemenov.snippets.domain.referenceBooks.Role.FIND_ROLE_BY_ID_QUERY;
import static com.isemenov.snippets.domain.referenceBooks.Role.FIND_ROLE_BY_NAME_QUERY;
import static com.isemenov.snippets.domain.referenceBooks.Role.FIND_ROLE_BY_CODE_QUERY;
import static com.isemenov.snippets.domain.referenceBooks.Role.FIND_ROLES_QUERY;

@NamedQueries({
        @NamedQuery(name = FIND_ROLE_BY_ID_QUERY, query = "from Role where id = :id"),
        @NamedQuery(name = FIND_ROLE_BY_NAME_QUERY, query = "from Role where name = :name"),
        @NamedQuery(name = FIND_ROLE_BY_CODE_QUERY, query = "from Role where code = :code"),
        @NamedQuery(name = FIND_ROLES_QUERY, query = "from Role")
    }
)
@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "roles")
public class Role extends ReferenceBook{

    public static final String FIND_ROLE_BY_ID_QUERY = "findRoleById";
    public static final String FIND_ROLE_BY_NAME_QUERY = "findRoleByName";
    public static final String FIND_ROLE_BY_CODE_QUERY = "findRoleByCode";
    public static final String FIND_ROLES_QUERY = "findAllRoles";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "role_id_generator")
    @SequenceGenerator(name = "role_id_generator", sequenceName = "role_id_seq")
    @Column(name = "role_id")
    private long roleId;

    public Role(String name, String code) {
        super(name, code);
    }
}
