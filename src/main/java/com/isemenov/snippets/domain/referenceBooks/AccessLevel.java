package com.isemenov.snippets.domain.referenceBooks;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

import static com.isemenov.snippets.domain.referenceBooks.AccessLevel.FIND_ACCESS_LEVEL_BY_ID_QUERY;
import static com.isemenov.snippets.domain.referenceBooks.AccessLevel.FIND_ACCESS_LEVEL_BY_NAME_QUERY;
import static com.isemenov.snippets.domain.referenceBooks.AccessLevel.FIND_ACCESS_LEVEL_BY_CODE_QUERY;
import static com.isemenov.snippets.domain.referenceBooks.AccessLevel.FIND_ACCESS_LEVELS_QUERY;

@NamedQueries({
        @NamedQuery(name = FIND_ACCESS_LEVEL_BY_ID_QUERY, query = "from AccessLevel where id = :id"),
        @NamedQuery(name = FIND_ACCESS_LEVEL_BY_NAME_QUERY, query = "from AccessLevel where name = :name"),
        @NamedQuery(name = FIND_ACCESS_LEVEL_BY_CODE_QUERY, query = "from AccessLevel where code = :code"),
        @NamedQuery(name = FIND_ACCESS_LEVELS_QUERY, query = "from AccessLevel")
    }
)
@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "access_levels")
public class AccessLevel extends ReferenceBook {

    public static final String FIND_ACCESS_LEVEL_BY_ID_QUERY = "findAccessLevelById";
    public static final String FIND_ACCESS_LEVEL_BY_NAME_QUERY = "findAccessLevelByName";
    public static final String FIND_ACCESS_LEVEL_BY_CODE_QUERY = "findAccessLevelByCode";
    public static final String FIND_ACCESS_LEVELS_QUERY = "findAllAccessLevels";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "access_level_id_generator")
    @SequenceGenerator(name = "access_level_id_generator", sequenceName = "access_level_id_seq")
    @Column(name = "access_level_id")
    private long accessLevelId;

    public AccessLevel(String name, String code) {
        super(name, code);
    }
}
