package com.isemenov.snippets.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

import static com.isemenov.snippets.domain.Tag.FIND_TAG_BY_ID_QUERY;
import static com.isemenov.snippets.domain.Tag.FIND_TAG_BY_NAME_QUERY;
import static com.isemenov.snippets.domain.Tag.FIND_TAGS_QUERY;

@NamedQueries({
        @NamedQuery(name = FIND_TAG_BY_ID_QUERY, query = "from Tag where id = :id"),
        @NamedQuery(name = FIND_TAG_BY_NAME_QUERY, query = "from Tag where name = :name"),
        @NamedQuery(name = FIND_TAGS_QUERY, query = "from Tag"),
    }
)
@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tags")
public class Tag {

    public static final String FIND_TAG_BY_ID_QUERY = "findTagById";
    public static final String FIND_TAG_BY_NAME_QUERY = "findTagByName";
    public static final String FIND_TAGS_QUERY = "findAllTags";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tag_id_generator")
    @SequenceGenerator(name = "tag_id_generator", sequenceName = "tag_is_seq")
    @Column(name = "tag_id")
    private long tagId;

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "snippets_tags",
            joinColumns = @JoinColumn(name = "tag_id"),
            inverseJoinColumns = @JoinColumn(name = "snippet_id")
    )
    private Collection<Snippet> snippets;

    public Tag(String name) {
        this.name = name;
        this.snippets = new ArrayList<>();
    }

}
