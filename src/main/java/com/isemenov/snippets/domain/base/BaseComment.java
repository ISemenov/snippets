package com.isemenov.snippets.domain.base;

import com.isemenov.snippets.domain.User;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@NoArgsConstructor
@MappedSuperclass
public class BaseComment {

    @Column(name = "message", nullable = false)
    private String message;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public BaseComment(String message, User user) {
        this.message = message;
        this.user = user;
    }
}
