package com.isemenov.snippets.domain;

import com.isemenov.snippets.domain.referenceBooks.Role;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static com.isemenov.snippets.domain.User.FIND_USER_BY_ID_QUERY;
import static com.isemenov.snippets.domain.User.FIND_USER_BY_LOGIN_QUERY;
import static com.isemenov.snippets.domain.User.FIND_USER_BY_EMAIL_QUERY;
import static com.isemenov.snippets.domain.User.FIND_USERS_QUERY;

@NamedQueries({
        @NamedQuery(name = FIND_USER_BY_ID_QUERY, query = "from User where id = :id"),
        @NamedQuery(name = FIND_USER_BY_LOGIN_QUERY, query = "from User where login = :login"),
        @NamedQuery(name = FIND_USER_BY_EMAIL_QUERY, query = "from User where email = :email"),
        @NamedQuery(name = FIND_USERS_QUERY, query = "from User")
    }
)
@Getter
@Setter
@Entity
@Table(name = "users")
public class User {

    public static final String FIND_USER_BY_ID_QUERY = "findUserById";
    public static final String FIND_USER_BY_LOGIN_QUERY = "findUserByLogin";
    public static final String FIND_USER_BY_EMAIL_QUERY = "findUserByEmail";
    public static final String FIND_USERS_QUERY = "findAllUsers";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_id_generator")
    @SequenceGenerator(name = "user_id_generator", sequenceName = "user_id_seq")
    @Column(name = "user_id")
    private long userId;

    @Column(name = "login", unique = true, nullable = false)
    private String login;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "email", unique = true, nullable = false)
    private String email;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "phone", unique = true)
    private String phone;

    @ManyToOne
    @JoinColumn(name = "role_id")
    private Role role;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    private List<Snippet> snippets;

    public User() {
        this.snippets = new ArrayList<>();
    }

    public User(String login, String password, String email) {
        this();
        this.login = login;
        this.password = password;
        this.email = email;
    }

    public User(String login, String password, String email, Role role) {
        this();
        this.login = login;
        this.password = password;
        this.email = email;
        this.role = role;
    }
}
