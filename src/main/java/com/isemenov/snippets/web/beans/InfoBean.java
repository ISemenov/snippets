package com.isemenov.snippets.web.beans;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class InfoBean {

    private String projectName;

    private String version;

    private String description;

    private List<String> authors;

    public InfoBean(String projectName, String version) {
        this.projectName = projectName;
        this.version = version;
        this.authors = new ArrayList<>();
    }
}
