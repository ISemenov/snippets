package com.isemenov.snippets.web.configuration.servlets;

import com.isemenov.snippets.web.beans.IndexPageBean;

import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/")
public class IndexServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        EntityManager em = (EntityManager) req.getServletContext().getAttribute("em");

        IndexPageBean indexPageBean = new IndexPageBean();
        req.setAttribute("indexPageBean", indexPageBean);
        req.getRequestDispatcher("/jsp/pages/index.jsp").forward(req, resp);
    }
}
