package com.isemenov.snippets.web.configuration.servlets;

import com.isemenov.snippets.web.beans.InfoBean;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/info")
public class InfoServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        InfoBean infoBean = new InfoBean("Snippets", "v.0.1");
        infoBean.getAuthors().add("Semenov Ivan");
        infoBean.setDescription(
                "Test project developed during " +
                "<a class=\"btn btn-link\" href='http://levelp.ru/' target='_blank' role=\"button\" >" +
                    "LevelUP &raquo;" +
                "</a>" +
                " lessons. "+
                "May be used as a template project in future."
        );
        req.setAttribute("infoBean", infoBean);
        req.getRequestDispatcher("/jsp/pages/info.jsp").forward(req, resp);
    }
}
