package com.isemenov.snippets.dao;

import com.isemenov.snippets.domain.Snippet;
import com.isemenov.snippets.domain.SnippetComment;
import com.isemenov.snippets.domain.Tag;
import com.isemenov.snippets.domain.User;
import com.isemenov.snippets.domain.referenceBooks.AccessLevel;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.junit.Assert.*;

public class SnippetsDAOTest {

    private EntityManagerFactory factory;
    private EntityManager em;
    private SnippetsDAO dao;
    private double delta = 0.0001;


    @Before
    public void setUp() throws Exception {
        factory = Persistence.createEntityManagerFactory("TestPersistenceUnit");
        em = factory.createEntityManager();
        dao = new SnippetsDAO(em);
    }

    @After
    public void tearDown() throws Exception {
        if(em!= null){
            em.close();
        }
        if(factory != null){
            factory.close();
        }
    }

    @Test
    public void testCreateSnippet() {

        User testUser = new User("testLogin", "testPassword", "testEmail");
        User testUser2 = new User("testLogin2", "testPassword2", "testEmail2");
        AccessLevel testAccessLevel = new AccessLevel("testAccessLevel", "testAccessLevel");

        dao.getManager().getTransaction().begin();
        dao.getManager().persist(testAccessLevel);
        dao.getManager().persist(testUser);
        dao.getManager().persist(testUser2);
        dao.getManager().getTransaction().commit();

        String testDescription = "testDescription";
        String testFileLink = "testFileLink";

        Snippet snippet = dao.createSnippet(testDescription, testFileLink, testAccessLevel, testUser);

        List<SnippetComment> comments = new ArrayList<>();
        comments.add(new SnippetComment("m1", testUser2, snippet));
        comments.add(new SnippetComment("m2", testUser2, snippet));

        dao.getManager().getTransaction().begin();

        snippet.setSnippetComments(comments);
        dao.getManager().getTransaction().commit();

        assertNotEquals(0L, snippet.getSnippetId());
        assertEquals(testDescription, snippet.getDescription());
        assertEquals(testFileLink, snippet.getFileLink());
        assertEquals(testAccessLevel, snippet.getAccessLevel());
        assertEquals(testUser, snippet.getUser());
        assertEquals(0D, snippet.getLikesRating(), delta);
        assertEquals(0D, snippet.getUsageRating(), delta);
    }

    @Test
    public void testDeleteSnippet() {

        User testUser = new User("testLogin", "testPassword", "testEmail");
        AccessLevel testAccessLevel = new AccessLevel("testAccessLevel", "testAccessLevel");

        dao.getManager().getTransaction().begin();
        dao.getManager().persist(testAccessLevel);
        dao.getManager().persist(testUser);
        dao.getManager().getTransaction().commit();

        Snippet snippet = dao.createSnippet("testDescription", "testFileLink", testAccessLevel, testUser);

        dao.deleteSnippet(snippet);

        assertNull(dao.getManager().find(Snippet.class, snippet.getSnippetId()));
        assertNotNull(dao.getManager().find(User.class, testUser.getUserId()));
        assertNotNull(dao.getManager().find(AccessLevel.class, testAccessLevel.getAccessLevelId()));

    }

    @Test
    public void testUpdateSnippet_AllFieldsChanged(){

        User testUser = new User("testLogin", "testPassword", "testEmail");
        AccessLevel testAccessLevel = new AccessLevel("testAccessLevel", "testAccessLevel");

        dao.getManager().getTransaction().begin();
        dao.getManager().persist(testAccessLevel);
        dao.getManager().persist(testUser);
        dao.getManager().getTransaction().commit();

        String testDescription = "testDescription";
        String testFileLink = "testFileLink";

        Snippet snippet = dao.createSnippet(testDescription, testFileLink, testAccessLevel, testUser);
        dao.getManager().close();

        EntityManager entityManager = factory.createEntityManager();
        SnippetsDAO snippetsDAO = new SnippetsDAO(entityManager);
        Snippet snippetToUpdate = snippetsDAO.findById(snippet.getSnippetId());

        String newDescription = "newDescription";
        String newFileLink = "newFileLink";

        User newUser = new User("newLogin", "newPassword", "newEmail");
        AccessLevel newAccessLevel = new AccessLevel("newAccessLevel", "newAccessLevel");

        List<Tag> tags = new ArrayList<>();
        tags.add(new Tag("testTag1"));
        tags.add(new Tag("testTag2"));

        List<SnippetComment> comments = new ArrayList<>();
        comments.add(new SnippetComment("m1", newUser, snippetToUpdate));
        comments.add(new SnippetComment("m2", newUser, snippetToUpdate));

        snippetsDAO.getManager().getTransaction().begin();
        snippetsDAO.getManager().persist(newAccessLevel);
//        comments.forEach(o->snippetsDAO.getManager().persist(o));
        snippetsDAO.getManager().persist(newUser);
        tags.forEach(o->snippetsDAO.getManager().persist(o));
        snippetsDAO.getManager().getTransaction().commit();

        snippetToUpdate.setDescription(newDescription);
        snippetToUpdate.setFileLink(newFileLink);
        snippetToUpdate.setAccessLevel(newAccessLevel);
        snippetToUpdate.setUser(newUser);
        snippetToUpdate.setLikesRating(1);
        snippetToUpdate.setUsageRating(1);
        snippetToUpdate.setTags(tags);
        snippetToUpdate.setSnippetComments(comments);

        snippetsDAO.updateSnippet(snippetToUpdate);

        assertEquals(snippet.getSnippetId(), snippetToUpdate.getSnippetId());
        assertEquals(newDescription, snippetToUpdate.getDescription());
        assertEquals(newFileLink, snippetToUpdate.getFileLink());
        assertEquals(newAccessLevel, snippetToUpdate.getAccessLevel());
        assertEquals(newUser, snippetToUpdate.getUser());
        assertEquals(1, snippetToUpdate.getLikesRating(), delta);
        assertEquals(1, snippetToUpdate.getUsageRating(), delta);
        assertEquals(tags, snippetToUpdate.getTags());
        assertEquals(comments, snippetToUpdate.getSnippetComments());

    }

    @Test
    public void testUpdateSnippet_SameSnippet_NoFieldsChanged() {

        User testUser = new User("testLogin", "testPassword", "testEmail");
        AccessLevel testAccessLevel = new AccessLevel("testAccessLevel", "testAccessLevel");

        dao.getManager().getTransaction().begin();
        dao.getManager().persist(testAccessLevel);
        dao.getManager().persist(testUser);
        dao.getManager().getTransaction().commit();

        Snippet snippet = dao.createSnippet("testDescription", "testFileLink", testAccessLevel, testUser);

        Snippet updatedSnippet = dao.updateSnippet(snippet);

        assertEquals(snippet.getSnippetId(), updatedSnippet.getSnippetId());
        assertEquals(snippet.getDescription(), updatedSnippet.getDescription());
        assertEquals(snippet.getFileLink(), updatedSnippet.getFileLink());
        assertEquals(snippet.getAccessLevel(), updatedSnippet.getAccessLevel());
        assertEquals(snippet.getUser(), updatedSnippet.getUser());
        assertEquals(snippet.getLikesRating(), updatedSnippet.getLikesRating(), delta);
        assertEquals(snippet.getUsageRating(), updatedSnippet.getUsageRating(), delta);
        assertEquals(snippet.getTags(), updatedSnippet.getTags());
        assertEquals(snippet.getSnippetComments(), updatedSnippet.getSnippetComments());

    }

    @Test
    public void testFindById() {

        User testUser = new User("testLogin", "testPassword", "testEmail");
        AccessLevel testAccessLevel = new AccessLevel("testAccessLevel", "testAccessLevel");

        dao.getManager().getTransaction().begin();
        dao.getManager().persist(testAccessLevel);
        dao.getManager().persist(testUser);
        dao.getManager().getTransaction().commit();

        Snippet snippet = dao.createSnippet("testDescription", "testFileLink", testAccessLevel, testUser);

        Snippet foundSnippet = dao.findById(snippet.getSnippetId());
        assertEquals(snippet, foundSnippet);
    }

    @Test
    public void testFindByUser() {

        User testUser = new User("testLogin", "testPassword", "testEmail");
        AccessLevel testAccessLevel = new AccessLevel("testAccessLevel", "testAccessLevel");

        dao.getManager().getTransaction().begin();
        dao.getManager().persist(testAccessLevel);
        dao.getManager().persist(testUser);
        dao.getManager().getTransaction().commit();

        Snippet snippet1 = dao.createSnippet("testDescription1", "testFileLink1", testAccessLevel, testUser);
        Snippet snippet2 = dao.createSnippet("testDescription2", "testFileLink2", testAccessLevel, testUser);

        List<Snippet> foundSnippets = dao.findByUser(testUser);
        assertFalse(foundSnippets.isEmpty());
        assertEquals(2, foundSnippets.size());
        foundSnippets.sort(Comparator.comparingLong(Snippet::getSnippetId));
        assertEquals(snippet1, foundSnippets.get(0));
        assertEquals(snippet2, foundSnippets.get(1));

    }
}