package com.isemenov.snippets.dao;

import com.isemenov.snippets.domain.Tag;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class TagsDAOTest {

    private EntityManagerFactory factory;
    private EntityManager em;
    private TagsDAO dao;

    @Before
    public void setUp() throws Exception {
        factory = Persistence.createEntityManagerFactory("TestPersistenceUnit");
        em = factory.createEntityManager();
        dao = new TagsDAO(em);
    }

    @After
    public void tearDown() throws Exception {
        if(em!= null){
            em.close();
        }
        if(factory != null){
            factory.close();
        }
    }

    @Test
    public void createTag() {
    }

    @Test
    public void deleteTag() {
    }

    @Test
    public void updateTag() {
    }

    @Test
    public void findById() {
    }

    @Test
    public void findByName() {
    }

    @Test
    public void findAll() {

        em.getTransaction().begin();
        List<Tag> tags = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            tags.add(new Tag("testTag" + i));
        }
        for(Tag tag: tags){
            em.persist(tag);
        }
        em.getTransaction().commit();
        em.close();

        EntityManager manager = factory.createEntityManager();
        manager.getTransaction().begin();
        List<Tag> tagsFromDB = new TagsDAO(manager).findAll();
        assertNotNull(tagsFromDB);
        assertFalse(tagsFromDB.isEmpty());
//        tagsFromDB.forEach(o-> System.out.println(o.getName()));
    }
}