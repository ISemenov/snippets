package com.isemenov.snippets.dao;

import com.isemenov.snippets.domain.Snippet;
import com.isemenov.snippets.domain.User;
import com.isemenov.snippets.domain.referenceBooks.AccessLevel;
import com.isemenov.snippets.domain.referenceBooks.Role;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class UsersDAOTest {

    private EntityManagerFactory factory;
    private EntityManager em;
    private UsersDAO dao;

    @Before
    public void setUp() throws Exception {
        factory = Persistence.createEntityManagerFactory("TestPersistenceUnit");
        em = factory.createEntityManager();
        dao = new UsersDAO(em);
    }

    @After
    public void tearDown() throws Exception {
        if(em!= null){
            em.close();
        }
        if(factory != null){
            factory.close();
        }
    }

    @Test
    public void testCreateUser() {
        User user = dao.createUser("testLogin", "testPassword", "testEmail");

        // DAO вернул правильный объект
        assertNotEquals(0L, user.getUserId());
        assertEquals("testLogin", user.getLogin());
        assertEquals("testPassword", user.getPassword());
        assertEquals("testEmail", user.getEmail());
        assertNotNull(user.getSnippets());
        assertTrue(user.getSnippets().isEmpty());

    }

    @Test
    public void testDeleteUser() {

        User user = dao.createUser("testLogin", "testPassword", "testEmail");

        User userFound = em.find(User.class, user.getUserId());
        dao.deleteUser(userFound);
        assertNull(em.find(User.class, user.getUserId()));
    }

    @Test
    public void testUpdateUser_SameUser_NoFieldsChanged(){

        User user = dao.createUser("testLogin", "testPassword", "testEmail");
        user.setFirstName("testFirstName");
        user.setLastName("testLastName");
        user.setPhone("testPhone");

        dao.updateUser(user);
        assertEquals(1L, user.getUserId());
        assertEquals("testLogin", user.getLogin());
        assertEquals("testPassword", user.getPassword());
        assertEquals("testEmail", user.getEmail());
        assertEquals("testFirstName", user.getFirstName());
        assertEquals("testLastName", user.getLastName());
        assertEquals("testPhone", user.getPhone());
        assertNull(user.getRole());
        assertTrue(user.getSnippets().isEmpty());
    }

    @Test
    public void testUpdateUser_AllFieldsExceptIdChanged() {

        User user = dao.createUser("testLogin", "testPassword", "testEmail");
        dao.getManager().getTransaction().begin();
        user.setFirstName("testFirstName");
        user.setLastName("testLastName");
        user.setPhone("testPhone");
        dao.getManager().getTransaction().commit();
        dao.getManager().close();

        EntityManager entityManager = factory.createEntityManager();
        UsersDAO usersDAO = new UsersDAO(entityManager);
        User userToUpdate = usersDAO.findById(user.getUserId());

        String newLogin = "newLogin";
        String newPassword = "newPassword";
        String newEmail = "newEmail";
        String newFirstName = "newFirstName";
        String newLastName = "newLastName";
        String newPhone = "newPhone";
        Role role = new Role("testRole", "testRole");
        entityManager.persist(role);
        List<Snippet> snippets = new ArrayList<>();
        AccessLevel testAccessLevel = new AccessLevel("testAccessLevel", "testAccessLevel");
        entityManager.persist(testAccessLevel);
        snippets.add(new Snippet("testDescription", "testFileLink", testAccessLevel, userToUpdate));

        userToUpdate.setLogin(newLogin);
        userToUpdate.setPassword(newPassword);
        userToUpdate.setEmail(newEmail);
        userToUpdate.setFirstName(newFirstName);
        userToUpdate.setLastName(newLastName);
        userToUpdate.setPhone(newPhone);
        userToUpdate.setRole(role);
        userToUpdate.setSnippets(snippets);

        usersDAO.updateUser(userToUpdate);

        assertEquals(user.getUserId(), userToUpdate.getUserId());
        assertEquals(newLogin, userToUpdate.getLogin());
        assertEquals(newPassword, userToUpdate.getPassword());
        assertEquals(newEmail, userToUpdate.getEmail());
        assertEquals(newFirstName, userToUpdate.getFirstName());
        assertEquals(newLastName, userToUpdate.getLastName());
        assertEquals(newPhone, userToUpdate.getPhone());
        assertEquals(role, userToUpdate.getRole());
        assertFalse(userToUpdate.getSnippets().isEmpty());
        assertEquals(1, userToUpdate.getSnippets().size());

    }

    @Test
    public void testFindById() {
        User user = dao.createUser("testLogin", "testPassword", "testEmail");

        User userFound = dao.findById(user.getUserId());
        assertEquals(user, userFound);
    }

    @Test
    public void testFindByLogin() {
        User user = dao.createUser("testLogin", "testPassword", "testEmail");

        User userFound = dao.findByLogin(user.getLogin());
        assertEquals(user, userFound);
    }

    @Test
    public void testFindByEmail() {
        User user = dao.createUser("testLogin", "testPassword", "testEmail");

        User userFound = dao.findByEmail(user.getEmail());
        assertEquals(user, userFound);
    }
}